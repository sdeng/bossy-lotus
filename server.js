var Twit = require('twit');
var twit = new Twit({
    consumer_key: '0mOZyareBqxVpHJQLUXwbQ',
    consumer_secret: 'cNLLfvcDt79h3Ksf7j3vM7TmJqhjJUosyXLSad5CzBc',
    access_token: '1646607966-qG9nEfsJQGda1moyhSlUAm383aomIbYD30BVlh5',
    access_token_secret: 'KjJFr9JA2jsgXdbejVn4sjzmwh4wkBcpVDYlLmgA'
});
var stream = twit.stream('user');

var DataMass = require('./datamass');
var datamass = new DataMass();

var apns = require('apn');
var PushNotification = require('./push-notification');
var push_notification = new PushNotification();
var notification_tokens = ['b7f232f865a594b6bf4ce184f3b140d8ed72bb323cec30a1e8c19a1600474939'];


// Parse tweet to datamass, send push notification, and update Tenno clients
stream.on('tweet', function(tweet) {
    datamass.validate_missions();

    var added_mission = datamass.add_mission(tweet);

    // Only push notify missions with rewards
    if (added_mission && added_mission.reward != null) {
        notification_tokens.forEach(function(token) {
            var notification = new apns.Notification();

            notification.device = new apns.Device(token);
            notification.expiry = Math.floor(Date.now() / 1000) + 1800;
            notification.alert = added_mission.reward;
            notification.sound = 'default';

            push_notification.apple.sendNotification(notification);
        });
    }

    datamass.get_missions(function(active_missions) {
        io.sockets.emit('mission', active_missions);
    });
});


// Hook up new Tenno connections, and register for push notification
var io = require('socket.io').listen(7000);

io.sockets.on('connection', function(socket) {
    datamass.get_missions(function(active_missions) {
        socket.emit('mission', active_missions);
    });

    // TODO: Persist registered devices in a database
    socket.on('register', function(data) {
        console.log(data);
    });
});
