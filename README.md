# Bossy Lotus

Realtime Warframe alerts server, with filtering and push notifications.

## Prerequisits

 * NodeJS 0.10.8
 * Redis 2.6

## Quick Start

 * redis-server config/redis.conf
 * npm install
 * node server.js
