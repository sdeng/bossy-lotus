module.exports = PushNotification;


function PushNotification() {
    var apns = require('apn');
    var apns_connection = new apns.Connection({
        cert: 'certs/apple/cert.pem',
        key: 'certs/apple/key.pem',
        passphrase: 'whyamisofat',
        gateway: 'gateway.push.apple.com',
        port: '2195',
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0
    });

    apns_connection.on('error', function(error) {
        console.log('Error with APNS: ' + error);
    });

    apns_connection.on('transmitted', function(notification) {
        console.log('Notification sent...');
        console.log(notification);
    });

    apns_connection.on('connected', function() {
        console.log('Connected to APNS.');
    });

    apns_connection.on('disconnected', function() {
        console.log('Disconnected to APNS.');
    });

    apns_connection.on('transmissionError', function(error_code, notification) {
        console.log('APNS transmission error: (' + error_code + ') ' + notification.toString());
    });

    return {
        apple: apns_connection
    }
}
