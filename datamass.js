module.exports = DataMass;


function DataMass() {
    var redis = require('redis');
    var client = redis.createClient();

    client.on('error', function(error) {
        console.log('Redis Error: ' + error);
    });

    var get_missions = function(callback) {
        client.smembers('active_missions', function(error, expirations) {
            var active_missions = [];

            // No missions
            if (expirations.length == 0) {
                callback(active_missions);
            }

            expirations.forEach(function(expiration, index) {
                var mission_key = 'mission:' + expiration;

                client.hgetall(mission_key, function(error, mission) {
                    active_missions.push(mission);
                    if (index == (expirations.length - 1)) {
                        callback(active_missions);
                    }
                });
            });
        });
    }

    var add_mission = function(tweet) {
        var text_segments = tweet.text.split(' - ');

        if (text_segments.length < 2) {
            return null;
        }

        // Parse tweet into mission object
        var created_date = new Date(tweet.created_at);
        var start_timestamp = created_date.getTime();
        var duration = Number(text_segments[1].trim().split('m')[0]) * 60 * 1000;
        var description = text_segments[0].trim();
        var mission = {
            'brief': description.split('):')[1].trim(),
            'planet': description.split(')')[0].split('(')[1],
            'location': description.split('(')[0].trim(),
            'expiration': start_timestamp + duration,
            'credits': text_segments[2].trim(),
            'reward': text_segments.length > 3 ? text_segments[3].trim() : null
        }

        // Store in Redis
        client.sadd('active_missions', mission.expiration);
        client.hmset(
            'mission:' + mission.expiration,
            'brief', mission.brief,
            'planet', mission.planet,
            'location', mission.location,
            'expiration', Number(mission.expiration),
            'credits', mission.credits
        );

        if (mission.reward != null) {
            client.hset(
                'mission:' + mission.expiration,
                'reward', mission.reward
            )
        }

        return mission;
    }

    var validate_missions = function() {
        get_missions(function(active_missions) {
            var now = Date.now();

            active_missions.forEach(function(mission, index) {
                if (Number(mission.expiration) > now) {
                    return;
                }

                client.srem('active_missions', mission.expiration);
                client.del('mission:' + mission.expiration);
            });
        });
    }

    return {
        get_missions: get_missions,
        add_mission: add_mission,
        validate_missions: validate_missions
    }
}
